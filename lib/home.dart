import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:assignmnet/common.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class DashBoard extends StatefulWidget {
  DashBoardState createState() => DashBoardState();
}

class DashBoardState extends State<DashBoard> {
  List listOfAllData = [];
  List imageUrl = [
    'https://i.pinimg.com/originals/b8/f3/64/b8f364483cf0217837fdabd6722870ac.png',
    'https://i.ytimg.com/vi/fLDXMibToy8/maxresdefault.jpg'
  ];
  bool isLoading = true;

  void initState() {
    getData();
    super.initState();
  }

  getData() async {
    await getAllDataList();
  }

  Future<void> getAllDataList() async {
    var response = await http.get('http://test.chatongo.in/testdata.json');
    if (response != null) {
      var responseBody = json.decode(response.body);
      int statusCode = 200;
      if (responseBody != null) {
        if (response.statusCode != null) {
          statusCode = response.statusCode;
        } else if (response.runtimeType == Map && responseBody != null) {
          statusCode = responseBody['Status'];
        }
      }
      var errorMessage = 'Something went wrong !';
      if (statusCode < 200 || statusCode > 399) {
        errorMessage = responseBody['message'];
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Assignment'),
                content: Text('Something went wrong'),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Ok"),
                    onPressed: () {
                      //Put your code here which you want to execute on Yes button click.
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            });
      }

      setState(() {
        listOfAllData = responseBody['data']['Records'];
        isLoading = false;
      });
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('DashBoard'),
      ),
      body: Platform.isAndroid
          ? isLoading
              ? loader()
              : ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  itemCount: listOfAllData.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext ctxt, int index) {
                    return Padding(
                        padding: EdgeInsets.all(10),
                        child: Container(
                            color: Colors.primaries[
                                Random().nextInt(Colors.primaries.length)],
                            padding: EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // CachedNetworkImage(
                                //   imageUrl:
                                //       'https://i.ytimg.com/vi/fLDXMibToy8/maxresdefault.jpg',
                                //   placeholder: (context, url) =>
                                //       CircularProgressIndicator(),
                                //   errorWidget: (context, url, error) => Icon(Icons.error),
                                // ),
                                AspectRatio(
                                  aspectRatio: 0.9,
                                  child: Carousel(
                                    images: imageUrl.map((url) {
                                      return CachedNetworkImage(
                                        imageUrl:
                                            'https://i.pinimg.com/originals/b8/f3/64/b8f364483cf0217837fdabd6722870ac.png',
                                      );
                                    }).toList(),
                                    dotSize: 4.0,
                                    dotSpacing: 15.0,
                                    dotBgColor: Colors.transparent,
                                    dotColor: Colors.red,
                                    autoplay: false,
                                  ),
                                ),
                                Text(
                                  listOfAllData[index]['title'],
                                  style: getTextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text('Short Description',
                                    style: getTextStyle(fontSize: 16)),
                                Text(
                                  listOfAllData[index]['shortDescription'],
                                  style: getTextStyle(fontSize: 16),
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Text(
                                  '₹' +
                                      listOfAllData[index]['collectedValue']
                                          .toString(),
                                  style: getTextStyle(fontSize: 16),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Text(
                                  '₹' +
                                      listOfAllData[index]['totalValue']
                                          .toString(),
                                  style: getTextStyle(fontSize: 16),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Start Date:-' +
                                          listOfAllData[index]['startDate'],
                                      style: getTextStyle(fontSize: 16),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    RaisedButton(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                      ),
                                      onPressed: () {},
                                      color: Colors.white,
                                      textColor: Colors.blue,
                                      child: Text("Pledge".toUpperCase(),
                                          style: TextStyle(fontSize: 14)),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'End Date:-' +
                                          listOfAllData[index]['endDate'],
                                      style: getTextStyle(fontSize: 16),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Icon(
                                      Icons.favorite,
                                      color: Colors.red,
                                      size: 30.0,
                                    ),
                                  ],
                                ),
                              ],
                            )));
                  },
                )
          : Container(),
    );
  }
}
