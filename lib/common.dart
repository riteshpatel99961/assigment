import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle getTextStyle(
    {double fontSize,
    FontWeight fontWeight,
    Color color,
    TextDecoration decoration,
    FontStyle fontStyle,
    double letterSpacing,
    List<Shadow> shadows,
    TextStyle textStyle,
    double decorationThickness,
    double height,
    double wordSpacing}) {
  return GoogleFonts.roboto(
      color: color,
      fontWeight: fontWeight,
      fontSize: fontSize,
      height: height,
      decorationThickness: decorationThickness,
      letterSpacing: letterSpacing,
      decoration: decoration,
      fontStyle: fontStyle,
      shadows: shadows,
      wordSpacing: wordSpacing);
}
loader() {
  return Container(
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(child: CircularProgressIndicator()),
    ),
  );
}